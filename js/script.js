
let dateTime = document.getElementById('date-time')
let startTime = document.getElementById('start')
let stopTime = document.getElementById('stop')
let totalMin = document.getElementById('minute')
let totalMoney = document.getElementById('money')
let btnStart = document.getElementById('btn-start')
let btnStop = document.getElementById('btn-stop')
let btnClear = document.getElementById('btn-clear')

const StopWatch = class {
    startTime;
    stopTime;
    start(){
        this.startTime = new Date();
        // innitailize 
        // this.startTime = new Date(2021, 04, 27, 10, 0, 30, 0);
    }

    stop(){
        this.stopTime = new Date();
        // innitailize 
        // this.stopTime = new Date(2021, 04, 27, 12, 59, 30, 0);
    }

    get startTimeString(){
        return this.startTime.toLocaleTimeString();
    }

    get stopTimeString(){
        return this.stopTime.toLocaleTimeString();
    }

    get totalTime (){
        const startMin = (this.startTime.getHours()*60)+this.startTime.getMinutes()
        const stopMin = (this.stopTime.getHours()*60)+this.stopTime.getMinutes()
        return stopMin - startMin;
    }
}

let stopWatch = new StopWatch();

function btnStartClick(){
    stopWatch.start();
    startTime.innerHTML= stopWatch.startTimeString;
    btnStart.style.display="none"
    btnStop.style.display="inherit"
}

function calculatePrice(time) {
    const listOfTime = [15, 30, 60];
    const listOfPrice = [500, 1000, 1500];
    let price = 0;
    for (let i = 0; i < listOfTime.length; i++) {
        if (time <= listOfTime[i] && time > 0) {
            price = listOfPrice[i];
            break;
        }
    }
    return price;
}

function calculateTotalPrice(totalTime){
    let result;
    if (totalTime > 60) {
        result = calculatePrice(60) + calculateTotalPrice(totalTime - 60);
    } else {
        result = calculatePrice(totalTime);
    }
    return result;
}

function btnStopClick(){
    stopWatch.stop();
    btnStop.style.display="none"
    btnClear.style.display="inherit"
    stopTime.innerHTML= stopWatch.stopTimeString;

    let calculateTime = stopWatch.totalTime;
    totalMin.innerHTML= calculateTime.toString()
    totalMoney.innerHTML = calculateTotalPrice(calculateTime);
}
function btnClearClick(){
    btnClear.style.display="none"
    btnStart.style.display="inherit"
    startTime.innerHTML = stopTime.innerHTML = "0:00"
    totalMin.innerHTML = totalMoney.innerHTML=  "0"
}

setInterval(()=> {
    let d= new Date();
    dateTime.innerHTML= d.toDateString() +" "+ d.toLocaleTimeString();
}, 1000);
        

